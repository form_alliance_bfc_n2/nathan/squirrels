
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to describe squirrels in Central Park NY

Ce package a été créé dans le cadre d’une formation N2.

## Installation

You can install the development version of squirrels like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
get_message_fur_color(primary_fur_color = "Black")
#> We will focus on Black squirrels
check_primary_color_is_ok(string = "Black")
#> [1] TRUE
```
